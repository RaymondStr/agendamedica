<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('clinicas', 'ClinicaController');
//rutas para el USUARIO DOCTOR
Route::resource('doctors', 'DoctorController');

Route::resource('users', 'UserController');

Route::resource('clientes', 'ClienteController');

Route::resource('citas', 'CitaController');

Route::resource('secretarias','SecretariaController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//ruta para el admin del sitio
Route::get('/adminDoctores', 'DoctorController@adminDoctorsIndex');
