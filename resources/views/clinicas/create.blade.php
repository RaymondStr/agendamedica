Seccion para crear clinicas
<br><br>

<form action="{{ url('/clinicas')}}" method="POST" enctype="multipart/form-data">

    {{ csrf_field() }}
    <label for="Nombre">{{'Nombre Clinica'}}</label>
    <input type="text" name="Nombre" id="Nombre" value="">
    <br>

    <label for="Calle">{{'Calle'}}</label>
    <input type="text" name="Calle" id="Calle" value="">
    <br>

    <label for="CodigoPostal">{{'Codigo Postal'}}</label>
    <input type="text" name="CodigoPostal" id="CodigoPostal" value="">
    <br>

    <label for="Fraccionamiento">{{'Fraccionamiento'}}</label>
    <input type="text" name="Fraccionamiento" id="Fraccionamiento" value="">
    <br>

    <label for="Numero">{{'Numero'}}</label>
    <input type="text" name="Numero" id="Numero" value="">
    <br>

    <label for="NumeroInterior">{{'Numero Interior'}}</label>
    <input type="text" name="NumeroInterior" id="NumeroInterior" value="">
    <br>

    <label for="Telefono">{{'Telefono'}}</label>
    <input type="text" name="Telefono" id="Telefono" value="">
    <br>

    <label for="CorreoClinica">{{'Correo Clinica'}}</label>
    <input type="email" name="CorreoClinica" id="CorreoClinica" value="">
    <br>

    <label for="Imagen">{{'Imagen Clinica'}}</label>
    <input type="file" name="Imagen" id="Imagen" value="">
    <br>

    <input type="submit" value="Agregar">

</form>
