@extends('layouts.app')

@section('content')
<h1 class="title text-center">Catalogo de Clinicas</h1>
<hr>
<div class="w-50 justify-content-center" style="width:50%; margin:auto;">
    <a href="{{route('clinicas.create')}}" class="btn btn-warning ">Agregar Clinica</a>
</div>
<hr>
<div class="row justify-content-center">
        <div class="col-auto">
            <table class="table table-striped table-inverse  " align="center">
                <thead class="thead-inverse">
                    <tr>
                        <th>Nombre Clinica</th>
                        <th>Calle</th>
                        <th>Cod. Postal</th>
                        <th>Fraccionamiento</th>
                        <th>Numero</th>
                        <th>Num. Interior</th>
                        <th>Telefono</th>
                        <th>Correo</th>
                        <th>Imagen de Clinica</th>
                        <th colspan="3">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($consultorios as $clinica)
                        <tr>
                                <td>{{ $clinica->nombre}}</td>
                                <td>{{ $clinica->calle}}</td>
                                <td>{{ $clinica->codigoPostal}}</td>
                                <td>{{ $clinica->fraccionamiento}}</td>
                                <td>{{ $clinica->numero}}</td>
                                <td>{{ $clinica->numeroInterior}}</td>
                                <td>{{ $clinica->telefono}}</td>
                                <td>{{ $clinica->correoClinica}}</td>
                                <td>{{ $clinica->imagen}}</td>
                                <td><a href="{{route('clinicas.edit',$clinica->id)}}" class="btn btn-success" >Editar</a></td>
                                <td><a href="{{route('clinicas.show',$clinica->id)}}" class="btn btn-primary" >Detalles</a></td>
                                <td><a href="{{route('clinicas.destroy',$clinica->id)}}" class="btn btn-danger" >Borrar</a></td>
                            </tr>
                        @endforeach
                    </tbody>
            </table>
        </div>
      </div>
@endsection
