@extends('layouts.app')

@section('content')
<h1 class="title text-center">Editar de Secretaria</h1>
<hr>
<form action="" method="POST">
    <div class="form-group" class="w-50 justify-content-center" style="width:25%; margin:auto;">
        <label for="name">{{'Nombre'}}</label>
    <input type="text" class="form-control" name="name" id="name" value="{{ $secre->name }}">

        <label for="apellidoPaterno">{{'Apellido Paterno'}}</label>
    <input type="text" class="form-control" name="apellidoPaterno" id="apellidoPaterno" value="{{ $secre->apellidoPaterno }}">

        <label for="apellidoMaterno">{{'Apellido Materno'}}</label>
        <input type="text" class="form-control" name="apellidoMaterno" id="apellidoMaterno" value="{{ $secre->apellidoMaterno }}">

        <label for="email">{{'Email'}}</label>
        <input type="email" class="form-control" name="email" id="email" value="{{ $secre->email }}">

        <label for="telefono">{{'Telefono'}}</label>
        <input type="text" class="form-control" name="telefono" id="telefono" value="{{ $secre->telefono }}">

        <label for="domicilio">{{'Domicilio'}}</label>
        <input type="text" class="form-control" name="domicilio" id="domicilio" value="{{ $secre->domicilio }}">
        <br>

        <button type="submit" class="btn btn-primary">Editar</button>

    </div>
 </form>
@endsection
