
@extends('layouts.app')

@section('content')
<h1 class="title text-center">Registro de Citas</h1>
<hr>
<form>
        <div class="form-group" class="w-50 justify-content-center" style="width:25%; margin:auto;">
          <label for="fechaCita">{{'Fecha de Cita'}}</label>
          <input type="date" class="form-control" name="fechaCita" id="fechaCita">

          <label for="HoraCita">{{'Hora de Cita'}}</label>
          <input type="time" class="form-control" name="HoraCita" id="HoraCita">
          <br>

          <button type="submit" class="btn btn-primary">Registrar</button>

        </div>
      </form>
@endsection
