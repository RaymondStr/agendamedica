Seccion de Index de Citas
@extends('layouts.app')

@section('content')
<h1 class="title text-center">Catalogo de Citas</h1>
<hr>
<div class="w-50 justify-content-center" style="width:50%; margin:auto;">
    <a href="{{route('citas.create')}}" class="btn btn-warning ">Agregar Cita</a>
</div>
<hr>
<div class="row justify-content-center">
        <div class="col-auto">
            <table class="table table-striped table-inverse  " align="center">
                <thead class="thead-inverse">
                    <tr>
                        <th>Eliga la Fecha</th>
                        <th>Eliga la Hora</th>
                        <th colspan="3">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($citaTomada as $cita)
                        <tr>
                                <td>{{ $cita->fechaCita}}</td>
                                <td>{{ $cita->fechaCita}}</td>
                                <td><a href="{{route('citas.edit',$cita->id)}}" class="btn btn-success" >Editar</a></td>
                                <td><a href="{{route('citas.show',$cita->id)}}" class="btn btn-primary" >Detalles</a></td>
                                <td><a href="{{route('citas.destroy',$cita->id)}}" class="btn btn-danger" >Borrar</a></td>
                            </tr>
                        @endforeach
                    </tbody>
            </table>
        </div>
      </div>
@endsection
