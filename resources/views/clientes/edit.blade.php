@extends('layouts.app')

@section('content')
<h1 class="title text-center">Editar de Clientes</h1>
<hr>
<form action="" method="POST">

        <div class="form-group" class="w-50 justify-content-center" style="width:25%; margin:auto;">
          <label for="name">{{'Nombre'}}</label>
        <input type="text" class="form-control" name="name" id="name" value="{{ $clientela->name }}">

          <label for="apellidoPaterno">{{'Apellido Paterno'}}</label>
          <input type="text" class="form-control" name="apellidoPaterno" id="apellidoPaterno" value="{{ $clientela->apellidoPaterno }}">

          <label for="apellidoMaterno">{{'Apellido Materno'}}</label>
          <input type="text" class="form-control" name="apellidoMaterno" id="apellidoMaterno" value="{{ $clientela->apellidoMaterno }}">

          <label for="email">{{'Email'}}</label>
          <input type="email" class="form-control" name="email" id="email" value="{{ $clientela->email }}">

          <label for="telefono">{{'Telefono'}}</label>
          <input type="text" class="form-control" name="telefono" id="telefono" value="{{ $clientela->telefono }}">

          <label for="domicilio">{{'Domicilio'}}</label>
          <input type="text" class="form-control" name="domicilio" id="domicilio" value="{{ $clientela->domicilio }}">

          <label for="tipo_sangre">{{'Tipo de Sangre'}}</label>
          <input type="text" class="form-control" name="tipo_sangre" id="tipo_sangre" value="{{ $clientela->tipo_sangre }}">

          <label for="alergias">{{'Alergias'}}</label>
          <input type="text" class="form-control" name="alergias" id="alergias" value="{{ $clientela->alergias }}">

          <label for="enfermedadesCronicas">{{'Enfermedades Cronicas'}}</label>
          <input type="text" class="form-control" name="enfermedadesCronicas" id="enfermedadesCronicas" value="{{ $clientela->enfermedadesCronicas }}">
          <br>

          <button type="submit" class="btn btn-primary">Registrar</button>

        </div>
      </form>
@endsection
