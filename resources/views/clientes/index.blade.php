@extends('layouts.app')

@section('content')
<h1 class="title text-center">Catalogo de Clientes</h1>
<hr>
<div class="w-50 justify-content-center" style="width:50%; margin:auto;">
    <a href="{{route('clientes.create')}}" class="btn btn-warning ">Agregar Cliente</a>
</div>
<hr>
<div class="row justify-content-center">
        <div class="col-auto">
            <table class="table table-striped table-inverse  " align="center">
                <thead class="thead-inverse">
                    <tr>
                        <th>Nombre</th>
                        <th>A. Paterno</th>
                        <th>A. Materno</th>
                        <th>Tipod de Sandre</th>
                        <th>Alergias</th>
                        <th>Enfermedades Cronicas</th>
                        <th colspan="3">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($clientela as $cliente)
                        <tr>
                                <td>{{ $cliente->name}}</td>
                                <td>{{ $cliente->apellidoPaterno}}</td>
                                <td>{{ $cliente->apellidoMaterno}}</td>
                                <td>{{ $cliente->tipo_sangre}}</td>
                                <td>{{ $cliente->alergias}}</td>
                                <td>{{ $cliente->enfermedadesCronicas}}</td>
                                <td><a href="{{route('clientes.edit',$cliente->id)}}" class="btn btn-success" >Editar</a></td>
                                <td><a href="{{route('clientes.show',$cliente->id)}}" class="btn btn-primary" >Detalles</a></td>
                                <td><a href="{{route('clientes.destroy',$cliente->id)}}" class="btn btn-danger" >Borrar</a></td>
                            </tr>
                        @endforeach
                    </tbody>
            </table>
        </div>
      </div>
@endsection
