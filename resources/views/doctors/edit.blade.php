@extends('layouts.app')

@section('content')
<h1 class="title text-center">Editar de Doctores</h1>
<hr>
<form action="" method="POST">

        <div class="form-group" class="w-50 justify-content-center" style="width:25%; margin:auto;">
                <label for="name">{{'Nombre'}}</label>
        <input type="text" class="form-control" name="name" id="name" value="{{ $doctores->name }}">

                <label for="apellidoPaterno">{{'Apellido Paterno'}}</label>
                <input type="text" class="form-control" name="apellidoPaterno" id="apellidoPaterno" value="{{ $doctores->apellidoPaterno }}">

                <label for="apellidoMaterno">{{'Apellido Materno'}}</label>
                <input type="text" class="form-control" name="apellidoMaterno" id="apellidoMaterno" value="{{ $doctores-> apellidoMaterno }}">

                <label for="email">{{'Email'}}</label>
                <input type="email" class="form-control" name="email" id="email" value="{{ $doctores->email }}">

                <label for="telefono">{{'Telefono'}}</label>
                <input type="text" class="form-control" name="telefono" id="telefono" value="{{ $doctores->telefono }}">

                <label for="domicilio">{{'Domicilio'}}</label>
                <input type="text" class="form-control" name="domicilio" id="domicilio" value="{{ $doctores->domicilio }}">

                <label for="especialidad">{{'Especialidad'}}</label>
                <input type="text" class="form-control" name="especialidad" id="especialidad" value="{{ $doctores->especialidad }}">

                <label for="numCedula">{{'Numero de Cedula'}}</label>
                <input type="text" class="form-control" name="numCedula" id="numCedula" value="{{ $doctores->numCedula }}">
                <br>

                <button type="submit" class="btn btn-primary">Registrar</button>

              </div>
</form>
@endsection
