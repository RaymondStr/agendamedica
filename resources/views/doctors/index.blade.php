@extends('layouts.app')

@section('content')
<h1 class="title text-center">Catalogo de Doctores</h1>
<hr>
<div class="w-50 justify-content-center" style="width:50%; margin:auto;">
    <a href="{{route('doctors.create')}}" class="btn btn-warning ">Agregar Doctor</a>
    <a href="{{route('clinicas.create')}}" class="btn btn-warning ">Agregar Clinica</a>
    <a href="{{route('secretarias.create')}}" class="btn btn-warning ">Agregar Secretaria</a>
</div>
<hr>
<div class="row justify-content-center">
        <div class="col-auto">
            <table class="table table-striped table-inverse  " align="center">
                <thead class="thead-inverse">
                    <tr>
                        <th>Nombre</th>
                        <th>A. Paterno</th>
                        <th>A. Materno</th>
                        <th>Especialidad</th>
                        <th>Num. Cedula</th>
                        <th colspan="3">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($doctores as $doctor)
                        <tr>
                                <td>{{ $doctor->name}}</td>
                                <td>{{ $doctor->apellidoPaterno}}</td>
                                <td>{{ $doctor->apellidoMaterno}}</td>
                                <td>{{ $doctor->especialidad}}</td>
                                <td>{{ $doctor->num_cedula}}</td>
                                <td><a href="{{route('doctors.edit',$doctor->id)}}" class="btn btn-success" >Editar</a></td>
                                <td><a href="{{route('doctors.show',$doctor->id)}}" class="btn btn-primary" >Detalles</a></td>
                                <td><a href="{{route('doctors.destroy',$doctor->id)}}" class="btn btn-danger" >Borrar</a></td>
                            </tr>
                        @endforeach
                    </tbody>
            </table>
        </div>
      </div>
@endsection
