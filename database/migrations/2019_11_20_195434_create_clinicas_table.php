<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClinicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinicas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('doctor_id');
            $table->unsignedBigInteger('cita_id');
            $table->unsignedBigInteger('secretaria_id');
            $table->string('nombre',45);
            $table->string('calle',255);
            $table->string('codigoPostal',5);
            $table->decimal('longitud', 11,8);
            $table->decimal('latitud', 10,8);
            $table->string('fraccionamiento', 255);
            $table->string('numero', 4);
            $table->string('numeroInterior', 2);
            $table->string('telefono', 10);
            $table->string('correoClinica', 255);
            $table->string('imagen',255);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinicas');
    }
}
