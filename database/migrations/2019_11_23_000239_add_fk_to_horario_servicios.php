<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkToHorarioServicios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('horario_servicios', function (Blueprint $table) {
            //
            $table->foreign('clinica_id')->references('id')->on('clinicas');
            $table->foreign('diaDeServicio_id')->references('id')->on('dia_servicios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('horario_servicio', function (Blueprint $table) {
            //
        });
    }
}
