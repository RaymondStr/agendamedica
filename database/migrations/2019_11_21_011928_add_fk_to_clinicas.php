<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkToClinicas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clinicas', function (Blueprint $table) {
            //
            $table->foreign('doctor_id')->references('id')->on('doctors');
            $table->foreign('cita_id')->references('id')->on('citas');
            $table->foreign('secretaria_id')->references('id')->on('secretarias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clinica', function (Blueprint $table) {
            //
        });
    }
}
