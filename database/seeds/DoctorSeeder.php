<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DoctorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('doctors')->insert( ['name'=>'Danielito Doctor',
                        'apellidoPaterno'=>'Cordova','apellidoMaterno'=>'Perez','email'=>
                        'DanielitoDoctor@gmail.com', 'domicilio'=>'Michoacan 126',
                        'telefono'=>'6622965478', 'especialidad'=>'Dentista',
                         'num_cedula'=>'123456789', 'user_id'=>'1', 'created_at' => Carbon::now()->toDateTimeString()],
        );

        DB::table('doctors')->insert( ['name'=>'Jorge Doctor',
        'apellidoPaterno'=>'Osuna','apellidoMaterno'=>'Castro','email'=>
        'JorgeDoctor@gmail.com', 'domicilio'=>'Emiliano Zapata 86',
        'telefono'=>'6622457812','especialidad'=>'Nutriologo', 'num_cedula'=> '987654321', 'user_id'=>'2', 'created_at' => Carbon::now()->toDateTimeString()],
        );

        DB::table('doctors')->insert( ['name'=>'Raymond Doctor',
        'apellidoPaterno'=>'Esparza','apellidoMaterno'=>'Castillo','email'=>
        'RaymondDoctor@gmail.com', 'domicilio'=>'Sostenes Rocha 404',
        'telefono'=>'6623457316','especialidad'=>'Psicologo','num_cedula'=> '123789456', 'user_id'=>'3', 'created_at' => Carbon::now()->toDateTimeString()],
        );

        DB::table('doctors')->insert( ['name'=>'Alonso Doctor',
        'apellidoPaterno'=>'Lopez','apellidoMaterno'=>'Romo','email'=>
        'AlonsoDoctor@gmail.com', 'domicilio'=>'Las Lomas 12',
        'telefono'=>'6623142864','especialidad'=>'Dermatologo', 'num_cedula'=>'456123789','user_id'=>'4','created_at' => Carbon::now()->toDateTimeString()],
        );
    }
}
