<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for ($i=0; $i < 10; $i++) {

            DB::table('users')->insert([
                'name' => Str::random(10),
                'apellidoPaterno' => Str::random(10),
                'apellidoMaterno' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'telefono' => Str::random(10),
                'domicilio' => Str::random(10),
                'password' => bcrypt('password'),
                'created_at' => Carbon::now()->toDateTimeString()
            ]);
        }
    }
}
