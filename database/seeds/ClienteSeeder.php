<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $nombreClientes = array("Danielito", "Jorge", "Ramon", "Alonso");
        $apellidoPatCliente = array("Cordova", "Osuna", "Esparza", "Lopez");
        $apellidoMatCliente = array("Perez", "Castro", "Castillo", "Romo");
        $telefonoCliente = array("6621478978", "6623457812", "6625459563", "6623128946");
        $emailCliente = array("danielin@gmail.com", "jorjais@gmail.com", "Raymond@gmail.com", "elTlaloc@gmail.com");
        $domicilioCliente = array("michoacan 126", "Emiliano Zapata 86", "Sostenes Rocha 404", "Las Lomas 12");
        $tiposSangre = array("O+", "O-","A+", "A-");
        $listaEfermedades = array("Enfermedades de Parkinson","Trastorno bipolar","Enfermedad de Crohn","Epilepsia", "Esclerosis multiple", "Demencia","VIH/SIDA", "Apnea del Sueño");
        $alergiasComunes = array("acaros", "polen","pelo de animales", "picaduras de insectos", "moho", "mariscos", "cacahuate", "latex", "Fragancias", "cucarachas");

        DB::table('clientes')->insert([
            'name'=> $nombreClientes[array_rand($nombreClientes)],
            'apellidoPaterno' => $apellidoPatCliente[array_rand($apellidoPatCliente)],
            'apellidoMaterno' => $apellidoMatCliente[array_rand($apellidoMatCliente)],
            'email' => $emailCliente[array_rand($emailCliente)],
            'domicilio' => $domicilioCliente[array_rand($domicilioCliente)],
            'telefono' => $telefonoCliente[array_rand($telefonoCliente)],
            'tipo_sangre' => $tiposSangre[array_rand($tiposSangre)],
            'alergias' => $alergiasComunes[array_rand($alergiasComunes)],
            'enfermedadesCronicas' => $listaEfermedades[array_rand($listaEfermedades)],
            'user_id' => User::all()->first()->id,
            'created_at' => Carbon::now()->toDateTimeString()
        ]);
    }
}
