<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    //
    protected $fillable = [
        'fechacita', 'horaCita', 'clinica_id', 'cliente_id',
    ];
}
