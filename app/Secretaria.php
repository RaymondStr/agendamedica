<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Secretaria extends User
{
    //
    protected $fillable = [
        'name', 'email', 'apellidoPaterno', 'apellidoMaterno', 'telefono', 'domicilio',
        'user_id',
    ];
}
