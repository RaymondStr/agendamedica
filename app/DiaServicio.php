<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiaServicio extends Model
{
    //
    protected $fillable = [
        'dia',
    ];
}
