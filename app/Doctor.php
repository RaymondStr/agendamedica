<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends User
{
    //
    protected $fillable = [
        'name', 'email', 'apellidoPaterno', 'apellidoMaterno', 'telefono', 'domicilio',
        'especialidad', 'num_cedula', 'user_id',
    ];
}
