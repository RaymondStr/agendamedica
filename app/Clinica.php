<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinica extends Model
{
    //
    protected $fillable = [
        'doctor_id', 'cita_id', 'secretaria_id', 'nombre', 'calle', 'codigoPostal', 'longitud',
        'latitud', 'fraccionamiento'. 'numero', 'numeroInterior', 'telefono', 'correoClinica',
        'imagen',
    ];
}
