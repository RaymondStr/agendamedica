<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HorarioServicio extends Model
{
    //
    protected $fillable = [
        'clinica_id', 'diaDeServicio_id',
    ];
}
