<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Cliente extends User
{

    protected $fillable = [
        'name', 'email', 'apellidoPaterno', 'apellidoMaterno', 'telefono', 'domicilio',
        'tipo_sangre', 'alergias', 'enfermedadesCronicas', 'user_id',
    ];

}
